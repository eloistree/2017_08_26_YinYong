﻿using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public struct RootObjectState
{
	public GameObject go;
	public bool wasActive;
}


public class startGame : MonoBehaviour
{

	public GameObject _moveCharacters;
	public GameObject _moveCharactersP2;

	public GameObject YinYang;
	public GameObject Buttons;

	public GameObject Title;

	public AudioSource backgroundMusic;

	public float fadeStart_title;

	static float t = 0.0f;

	float lerpTime = 2f;
	float currentLerpTime;

	public GameObject Left;
	public Vector3 startPos_L = new Vector3(14.76f, -5.35f, 0);
	public Vector3 endPos_L = new Vector3(5.89f, -5.35f, 0);

	public GameObject Right;
	public Vector3 startPos_R = new Vector3(14.76f, -5.35f, 0);
	public Vector3 endPos_R = new Vector3(5.89f, -5.35f, 0);

	public bool TriggerStart = false;
	public float timeleft = 2f;

	SpriteRenderer Title_color;
	moveCharacters stg;
	moveCharacters stgP2;

	public List<RootObjectState> SpawnerScene_Initial;


	// Use this for initialization
	void Start()
	{
		backgroundMusic.Play();
		Title_color = YinYang.GetComponent<SpriteRenderer>();
		stg = _moveCharacters.GetComponent<moveCharacters>();
		stgP2 = _moveCharactersP2.GetComponent<moveCharacters>();

		//SceneManager.sceneLoaded += delegate(Scene scene, LoadSceneMode lsm)
		//{
		//	if (scene.name == "Spawner")
		//	{
		//		scene.GetRootGameObjects();
		//		var tmp = scene.GetRootGameObjects();

		//		SpawnerScene_Initial = new List<RootObjectState>();
		//		foreach (var go in tmp)
		//		{
		//			var ros = new RootObjectState();
		//			ros.go = go;
		//			ros.wasActive = go.activeSelf;
		//			SpawnerScene_Initial.Add(ros);

		//			go.SetActive(false);
		//		}
		//	}
		//};
		//
	}
    

	void Update()
	{
        


		if (Input.GetKeyDown(KeyCode.P))
		{
			SceneManager.LoadScene("Factory");
		}

		if (Time.fixedTime > 2.5f)
		{
			Title.SetActive(true);


			if (!TriggerStart) Title_color.color = new Color(1f, 1f, 1f, Mathf.Lerp(0, 1, t));
			t += 0.5f * Time.deltaTime;
		}

		if (Time.fixedTime > 6f) Buttons.SetActive(true);
        bool rewiredRead = ReInput.players.GetPlayer(0).GetAnyButton() && ReInput.players.GetPlayer(1).GetAnyButton();
        bool keyboardRead = Input.GetKey("up") && Input.GetKey("down");
        if (rewiredRead || keyboardRead)
		{
			Title.SetActive(true);
			Buttons.SetActive(true);
			Title_color.color = new Color(1f, 1f, 1f, 1f);

			stg.startGameOk = true;

			stgP2.startGameOk = true;

			TriggerStart = true;
		}

		if (TriggerStart)
		{
			timeleft -= Time.deltaTime;

			if (timeleft < 0)
			{
				animationGame();
			}
		}

	}

	private bool firstTime = true;
	void animationGame()
	{
		if (firstTime)
		{
            // ???? LOOK AFTER
			firstTime = false;
			foreach (var ros in SpawnerScene_Initial)
			{
				if (ros.go)
					ros.go.SetActive(ros.wasActive);
			}
		}

		Title.SetActive(false);

		stg.startGameOk = true;
		stgP2.startGameOk = true;
		stg.shakeAnim = false;
		stgP2.shakeAnim = false;
		stg.scream.volume -= 0.6f * Time.deltaTime;
		stgP2.scream.volume -= 0.6f * Time.deltaTime;
		backgroundMusic.volume -= 0.2f * Time.deltaTime;

		currentLerpTime += Time.deltaTime;

		if (currentLerpTime > lerpTime)
		{
			currentLerpTime = lerpTime;
			enabled = false;
			SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
		}

		//lerp!
		float t = currentLerpTime / lerpTime;
		//t = t * t * (3f - 2f * t);
		t = Mathf.SmoothStep(0, 1, t);
		Right.transform.position = Vector3.Lerp(startPos_R, endPos_R, t);
		Left.transform.position = Vector3.Lerp(startPos_L, endPos_L, t);
        SceneManager.LoadScene("Spawner", LoadSceneMode.Single);
    }
}
