﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBrowser : MonoBehaviour {

    public void OpenBrowserAt(string url) {
        Application.OpenURL(url);
    }
}
