﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawning : MonoBehaviour {


    public static MobSpawning InstanceInScene { get { return _instance; } }
    private static MobSpawning _instance;

    public void Awake()
    {
        _instance = this;

    }

    public EnemyPrefab[] enemyType;

    [System.Serializable]
    public class EnemyPrefab
    {
        public string _id;
        public string _name;
        public GameObject _prefab;
    }



    public void CreateMob(string id, Vector3 position, Quaternion direction, YingYong team) {

        EnemyPrefab mobToCreate=null;
        for (int i = 0; i < enemyType.Length; i++)
        {
            if (enemyType[i]._id == id) {
                mobToCreate = enemyType[i];
                break;
            }
        }
        if (mobToCreate == null)
        {
            Debug.Log("Try to create an enemy that do not exist: " + id);
            return;
        }
        
        GameObject mobCreate = Instantiate(mobToCreate._prefab, position, direction);
        mobCreate.name = "> Enemy (" + name + ")";
        TeamAffected[] toAffect =mobCreate.GetComponentsInChildren<TeamAffected>();
        for (int i = 0; i < toAffect.Length; i++)
        {
            toAffect[i].SetTeam(team);
        }






    }
    

    void Start () {
		
	}
	
	void Update () {
		
	}
}
