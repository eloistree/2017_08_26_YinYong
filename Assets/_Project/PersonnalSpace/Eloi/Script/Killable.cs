﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Killable : MonoBehaviour, IKillabe
{
    public GameObject[] _toDestroyOnKill;
    public UnityEvent _onKillRequired;

    public void KillIt()
    {
        _onKillRequired.Invoke();
        for (int i = 0; i < _toDestroyOnKill.Length; i++)
        {
            Destroy(_toDestroyOnKill[i],1f);
        }
    }
}
public interface IKillabe
{
    void KillIt();
}