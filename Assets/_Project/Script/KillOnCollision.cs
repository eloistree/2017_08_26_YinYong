﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnCollision : MonoBehaviour {

    public LayerMask _dieInCollisionWith;
    public Killable _killable;
    
    public void OnCollisionEnter(Collision collision)
    {
        print("Hi");
        if (!(_dieInCollisionWith == (_dieInCollisionWith | (1 << collision.gameObject.layer))))
            return;


        print("Hello");
        if (_killable != null)
        {
            _killable.KillIt();
        }

    }
}
