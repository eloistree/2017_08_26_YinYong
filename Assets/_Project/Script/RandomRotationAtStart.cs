﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotationAtStart : MonoBehaviour {

    public Transform _affected;

    public void Awake() {
        _affected.rotation = Quaternion.Euler( new Vector3(0,Random.Range(0f,360f),0) );
    }


	void OnValidate ()
    {
        UseOwnTransform();
    }
    void Reset()
    {
        UseOwnTransform();
    }
    private void UseOwnTransform()
    {
        if (_affected == null)
        {
            _affected = transform;
        }
    }
    
}
