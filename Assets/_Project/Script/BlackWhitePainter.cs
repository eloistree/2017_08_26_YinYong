﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackWhitePainter : MonoBehaviour
{
	private MainScript mainScript;

	public GameObject PaintGameObject;
	[SerializeField] private GameObject psSplash;

	void Start()
	{
		mainScript = MainScript.Instance;
	}

	void Update()
	{

		if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
		{

			// TODO: Out ssphere on mouse pos!
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit))
			{
				var go = Instantiate(psSplash, this.transform);
				go.transform.position = hit.point;

				PaintGameObject.SetActive(true);
				PaintGameObject.transform.position = hit.point;

				mainScript.Execute_CreateBlood(Input.GetMouseButtonDown(0) ? 0 : 1);
				// Do something with the object that was hit by the raycast.
			}
		}
	}

	IEnumerator HidePaintGameObject()
	{
		yield return new WaitForSeconds(0.1f);
		PaintGameObject.SetActive(false);
	}

}
