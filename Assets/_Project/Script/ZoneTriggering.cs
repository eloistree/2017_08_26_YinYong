﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoneTriggering : MonoBehaviour, TeamAffected {

    public YingYong _belongTo;
    public bool _teamDefined;
    public bool IsTeamBeenDefined()
    {
        return _teamDefined;
    }
    public Transform _positionTracked;
    public float checkDuration=0.1f;

    public UnityEvent onEnterEnemyZone;
    public UnityEvent onEnterAllyZone;

    [Header("Debug")]
    public YingYong _groundOn;


    public IEnumerator Start()
    {
        while (true) {

            yield return new WaitForSeconds(checkDuration);
            CheckZoneIn();
        }
    }

    private void CheckZoneIn()
    {
        if (!_teamDefined)
            return;
          YingYong colorOnSpot = GetColorOnSpot(_positionTracked);
        YingYong fromColor = _groundOn;

        if (fromColor != colorOnSpot)
        {

            if (colorOnSpot != _belongTo)
            {
                onEnterEnemyZone.Invoke();

            }
            if (colorOnSpot == _belongTo)
            {
                onEnterAllyZone.Invoke();

            }

        }


        _groundOn = colorOnSpot;
    }

    private static YingYong GetColorOnSpot(Transform where)
    {
        return GetColorOnSpot(where.position);
    }
    private static YingYong GetColorOnSpot(Vector3 position)
    {
        float normX = Mathf.Clamp01(position.x / 17.7777f + 0.5f);
        float normY = Mathf.Clamp01(position.z / 10 + 0.5f);
        float color = MainScript.Instance.GetColor(normX, normY);
        return color < 0.5 ? YingYong.Black : YingYong.White;
    }

    public YingYong GetTeam()
    {
        return _belongTo;
    }
    public void SetTeam(YingYong team)
    {
        _teamDefined = true;
        _belongTo = team;
    }
}
