﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour {

    public GameObject [] _toActivate;
    public float _delayBefore = 5f;
    public string _sceneName="";
    public bool _launchAtStart;
    public void Start()
    {
        if (_launchAtStart)
            ReloadSceneInSomeTime();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(_sceneName, LoadSceneMode.Single);

    }
    public void ReloadSceneInSomeTime()
    {
        Invoke("ReloadScene", _delayBefore);

    }

}
