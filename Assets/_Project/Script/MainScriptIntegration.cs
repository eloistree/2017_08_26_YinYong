﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class MainScriptIntegration : MonoBehaviour
{

	public GameObject left;
	public GameObject right;

	public GameObject mousePos;

	private MainScript mainScript;

	void Start()
	{
		mainScript = MainScript.Instance;
		/*left.SetActive(true);
		right.SetActive(false);
		mainScript.Execute_CreateBlood(1);

		left.SetActive(false);
		right.SetActive(true);
		mainScript.Execute_CreateBlood(0);
		right.SetActive(false);*/
	}

	void Update()
	{
		//var caret = mousePos.transform.position;
		//caret
		if (Input.GetMouseButtonDown(3))
		{
			var pos = Input.mousePosition;
			//Debug.Log(Input.mousePosition.x + " " + Screen.width + " " + mainScript.width);
			/*pos.x /=  Screen.width;
			pos.y = pos.y * Screen.height;
			pos.x *= mainScript.width;
			pos.y = pos.y / mainScript.height;
			Debug.Log(pos);*/
			mainScript.QueueClusterDetection(pos);
		}
	}
    public static void SaveTexture(string path, RenderTexture tex)
	{
		var prevTex = RenderTexture.active;
		RenderTexture.active = tex;

		Texture2D virtualPhoto = new Texture2D(tex.width, tex.height, TextureFormat.RGB24, false);
		virtualPhoto.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
		SaveTexture(path, virtualPhoto);

		RenderTexture.active = prevTex;
	}

	public static void SaveTexture(string path, Texture2D tex)
	{
		var bytes = tex.EncodeToPNG();
		string finalPath = string.IsNullOrEmpty(path)
            ? Application.persistentDataPath + "/" + DateTime.Now.ToString("yyyymmdd_HH_MM_ss") + ".png"
            :path;
		System.IO.File.WriteAllBytes(finalPath, bytes);
	}
}
