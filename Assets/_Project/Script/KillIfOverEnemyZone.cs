﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KillIfOverEnemyZone : MonoBehaviour, TeamAffected
{

    Transform tr;
    YingYong team;
    public UnityEvent onExplode;

    public YingYong Team
    {
        set
        {
            team = value;
        }
    }
    public bool _teamDefined;
    public bool IsTeamBeenDefined()
    {
        return _teamDefined;
    }


    void Start () {
        tr = transform;
	}
	
	void Update () {
        Vector3 position = tr.position;
        float normX = Mathf.Clamp01(position.x / 17.7777f + 0.5f);
        float normY = Mathf.Clamp01(position.z / 10 + 0.5f);
        float color = MainScript.Instance.GetColor(normX, normY);
        if (color < 0.5 != (team == YingYong.Black))
        {
            onExplode.Invoke();

        }
    }

    public void SetTeam(YingYong team)
    {
        _teamDefined = true;
        this.team = team;
    }

    public YingYong GetTeam()
    {
        return team;
    }
}
