﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainScript : MonoBehaviour
{

	[SerializeField]
	private Shader renderShader;
	[SerializeField]
	private ComputeShader cshader;

    [SerializeField]
    private Texture2D[] startTexture;

	private RenderTexture texBuffer;

	private Material effectMaterial;

	private ComputeBuffer blackwhiteBuffer;
	private ComputeBuffer blackwhiteBufferTmp;

	private RenderTexture bloodBuffer;
	private RenderTexture bloodBufferTmp;


	private RenderTexture propagateBuffer;
	private RenderTexture propagateBufferTmp;

	[SerializeField]
	public int width = 16 * 64;
	[SerializeField]
	public int height = 9 * 64;

    [SerializeField]
    float blurSpeed = 5;
    [SerializeField]
    float bloodDisapearspeed = 0.2f;


	private int kernelCreateBlood;
	private int kernelUpdateBloodTexture;
	private int kernelAStarPropagateForBlack;
	private int kernelAStarPropagateForBlack_PotentialCleanup;

	private uint x, y, z;

	[SerializeField]
	private RawImage imageoutput;

	[SerializeField]
	private Camera bloodCamera;

	private RenderTexture bloodTextureFromRenderTexture;

	//private bool stepbw = false;
	//private bool stepBlood = false;

	float[] buffer;


    public bool captureScreenAtEachExplosion;
	private static MainScript instance;

	public static MainScript Instance
	{
		get
		{
			if (instance == null)
			{
				Debug.LogError("You should insert a MainScript in your scene");
			}
			return instance;
		}
	}


	private void Awake()
	{
		instance = this;
	}

	private IEnumerator Start()
	{

		//buffer creation
		blackwhiteBuffer = new ComputeBuffer(width * height, sizeof(float));
		blackwhiteBufferTmp = new ComputeBuffer(width * height, sizeof(float));

        cshader.SetInt("width", width); 
        cshader.SetInt("height", height);

		buffer = new float[width * height];
        byte[] startBuffer = new byte[width * height*4];

        for (int xx = 0; xx < width; xx++)
		{
			for (int yy = 0; yy < height; yy++)
			{
                buffer[xx + yy * width] = (xx < width / 2) ? 0 : 1;
                //if((xx + yy * width) % 99 == 0)
                //    Debug.Log(startBuffer[(xx + yy * width)]);
            }
		}

		blackwhiteBuffer.SetData(buffer);
		blackwhiteBufferTmp.SetData(buffer);


		texBuffer = new RenderTexture(width, height, 0);
		texBuffer.enableRandomWrite = true;
		texBuffer.Create();

		bloodTextureFromRenderTexture = new RenderTexture(width, height, 0);
		bloodTextureFromRenderTexture.enableRandomWrite = true;
		bloodTextureFromRenderTexture.Create();

		bloodCamera.targetTexture = bloodTextureFromRenderTexture;
		bloodCamera.enabled = false;

		bloodBuffer = new RenderTexture(width, height, 0);
		bloodBuffer.enableRandomWrite = true;
		bloodBuffer.Create();

		bloodBufferTmp = new RenderTexture(width, height, 0);
		bloodBufferTmp.enableRandomWrite = true;
		bloodBufferTmp.Create();

		// ///////////////////////////////////////////////////////////////////

		propagateBuffer = new RenderTexture(width, height, 0);
		propagateBuffer.enableRandomWrite = true;
		propagateBuffer.Create();

		propagateBufferTmp = new RenderTexture(width, height, 0);
		propagateBufferTmp.enableRandomWrite = true;
		propagateBufferTmp.Create();

		// ///////////////////////////////////////////////////////////////////



		//Kernel setup
		kernelCreateBlood = cshader.FindKernel("CreateBlood");
		kernelUpdateBloodTexture = cshader.FindKernel("UpdateBloodTexture");
		kernelAStarPropagateForBlack = cshader.FindKernel("AStarPropagateForBlack");
		kernelAStarPropagateForBlack_PotentialCleanup = cshader.FindKernel("AStarPropagateForBlack_PotentialCleanup");

		cshader.GetKernelThreadGroupSizes(kernelCreateBlood, out x, out y, out z);

		cshader.SetTexture(kernelCreateBlood, "Result", texBuffer);
		cshader.SetTexture(kernelUpdateBloodTexture, "Result", texBuffer);

        cshader.SetFloat("blurSpeed", blurSpeed);
        cshader.SetFloat("bloodDisapearspeed", bloodDisapearspeed);


		//Image effect setup
		effectMaterial = new Material(renderShader);
		effectMaterial.SetTexture("_BlackWhiteBuffer", texBuffer);


		Execute_AStarPropagateForBlack();
		effectMaterial.SetTexture("_PropagateBuffer", propagateBuffer);
		Execute_UpdateBloodTexture();

        Execute_CreateBlood(1);

        yield return null;

        //startTexture.LoadRawTextureData(startBuffer);
        startBuffer = startTexture[Random.Range(0,startTexture.Length)].GetRawTextureData();

        //Debug.Log(startBuffer.Length);
        //Debug.Log(width* height);
        //Debug.Log((float) startBuffer.Length / (width * height));

        for (int xx = 0; xx < width; xx++)
        {
            for (int yy = 0; yy < height; yy++)
            {
                buffer[xx + yy * width] = (float) startBuffer[(xx + yy * width)*4] /255f;
                /*if ((xx + yy * width) % 99 == 0)
                    Debug.Log(startBuffer[(xx + yy * width)]);*/
            }
        }

        blackwhiteBuffer.SetData(buffer);
        blackwhiteBufferTmp.SetData(buffer);

    }

    public void UpdateDeltaTime()
    {
        cshader.SetFloat("deltaTime", Time.deltaTime);
    }

	/*void Start()
	{
		Execute_CreateBlood(1);
	}*/


	public static GameObject[] FindGameObjectsWithLayer(int layer)
	{
		var goArray = FindObjectsOfType<GameObject>();
		var goList = new System.Collections.Generic.List<GameObject>();
		foreach (GameObject t in goArray)
		{
			if (t.layer == layer)
			{
				goList.Add(t);
			}
		}
		/*if (goList.Count == 0)
		{
			return null;
		}*/
		return goList.ToArray();
	}

	public float GetColor(float x, float y)
	{
        int xint = Mathf.Clamp(Mathf.CeilToInt(x * width), 0, width-1);
        int yint = Mathf.Clamp(Mathf.CeilToInt(y * height), 0, height-1);

        return buffer[xint + yint * width];
	}

	public void Execute_CreateBlood(int team)
	{
		bloodCamera.Render(); // to bloodTextureFromRenderTexture
		cshader.SetTexture(kernelCreateBlood, "bloodTextureFromRenderTexture", bloodTextureFromRenderTexture);
		cshader.SetTexture(kernelCreateBlood, "bloodTextureRead", bloodBuffer);
		cshader.SetTexture(kernelCreateBlood, "bloodTextureWrite", bloodBufferTmp);
		cshader.SetBuffer(kernelCreateBlood, "blackwhiteBufferRead", blackwhiteBuffer);
		cshader.SetBuffer(kernelCreateBlood, "blackwhiteBufferWrite", blackwhiteBufferTmp);
		cshader.SetFloat("teamnbr", team);
		cshader.Dispatch(kernelCreateBlood, (int)(width / x), (int)(height / y), 1);
		SwapBlackwhiteBuffer();
		SwapBloodTexture();
		imageoutput.texture = texBuffer;
        textureBuffer = texBuffer;

		this.blackwhiteBuffer.GetData(buffer);
		effectMaterial.SetTexture("_BloodBuffer", bloodBuffer);
	}
    //Used to take screenshot
    public RenderTexture textureBuffer;
    public void TakeScreenshot(string path) {
        MainScriptIntegration.SaveTexture(path,textureBuffer);
    }


	private void Execute_UpdateBloodTexture()
	{
		cshader.SetTexture(kernelUpdateBloodTexture, "bloodTextureRead", bloodBuffer);
		cshader.SetTexture(kernelUpdateBloodTexture, "bloodTextureWrite", bloodBufferTmp);
		cshader.SetBuffer(kernelUpdateBloodTexture, "blackwhiteBufferRead", blackwhiteBuffer);
		cshader.SetBuffer(kernelUpdateBloodTexture, "blackwhiteBufferWrite", blackwhiteBufferTmp);
        UpdateDeltaTime();
		cshader.Dispatch(kernelUpdateBloodTexture, (int)(width / x), (int)(height / y), 1);
		SwapBloodTexture();
		SwapBlackwhiteBuffer();

		effectMaterial.SetTexture("_BloodBuffer", bloodBuffer);
	}


	/// <summary>
	/// Call after each Dispatch that writes to the buffer!
	/// </summary>
	private void SwapBloodTexture()
	{
		var tmp = bloodBufferTmp;
		bloodBufferTmp = bloodBuffer;
		bloodBuffer = tmp;
	}
	/// <summary>
	/// Call after each Dispatch that writes to the buffer!
	/// </summary>
	private void SwapBlackwhiteBuffer()
	{
		var tmp = blackwhiteBufferTmp;
		blackwhiteBufferTmp = blackwhiteBuffer;
		blackwhiteBuffer = tmp;
	}

	/// <summary>
	/// Call after each Dispatch that writes to the buffer!
	/// </summary>
	private void SwapPropagateBuffer()
	{
		var tmp = propagateBufferTmp;
		propagateBufferTmp = propagateBuffer;
		propagateBuffer = tmp;
	}

	public int NumSwitchPropagateBuffer = 0; // How many times we switched yet?
	public void Execute_AStarPropagateForBlack()
	{
		cshader.SetTexture(kernelAStarPropagateForBlack, "propagateBufferRead", propagateBuffer);
		cshader.SetTexture(kernelAStarPropagateForBlack, "propagateBufferWrite", propagateBufferTmp);
		cshader.SetBuffer(kernelAStarPropagateForBlack, "blackwhiteBufferRead", blackwhiteBuffer);
		cshader.Dispatch(kernelAStarPropagateForBlack, (int)(width / x / 1), (int)(height / y / 1), 1); // Could optimise?
		SwapPropagateBuffer();
	}

	public void Execute_AStarPropagateForBlack_PotentialCleanup()
	{
		cshader.SetTexture(kernelAStarPropagateForBlack_PotentialCleanup, "propagateBufferRead", propagateBuffer);
		cshader.SetTexture(kernelAStarPropagateForBlack_PotentialCleanup, "propagateBufferWrite", propagateBufferTmp);
		cshader.SetBuffer(kernelAStarPropagateForBlack_PotentialCleanup, "blackwhiteBufferRead", blackwhiteBuffer);
		cshader.SetBuffer(kernelAStarPropagateForBlack_PotentialCleanup, "blackwhiteBufferWrite", blackwhiteBufferTmp);
		cshader.Dispatch(kernelAStarPropagateForBlack_PotentialCleanup, (int)(width / x / 1), (int)(height / y / 1), 1); // Could optimise?
		SwapPropagateBuffer();
		SwapBlackwhiteBuffer();
	}

	private readonly Queue<Vector2> clusterDetectionQueue = new Queue<Vector2>();
	public void QueueClusterDetection(Vector2 position)
	{
		clusterDetectionQueue.Enqueue(position);
	}

	void Update()
	{
		Execute_UpdateBloodTexture();


		if (this.clusterDetectionQueue.Count > 0)
		{
			var pos = this.clusterDetectionQueue.Peek();
			for (int i = 0; i < 40; i++)
			{
				NumSwitchPropagateBuffer += 1;
				cshader.SetVector("clusterDetectionStart", new Vector4(pos.x, pos.y, 0, 0));
				Execute_AStarPropagateForBlack();
			}
			if (NumSwitchPropagateBuffer > width * 1.6)
			{
				NumSwitchPropagateBuffer = 0;
				Execute_AStarPropagateForBlack_PotentialCleanup();
				Execute_CreateBlood(1);
				this.clusterDetectionQueue.Dequeue();
			}

			effectMaterial.SetTexture("_PropagateBuffer", propagateBuffer);
			//bSwitchPropagateBuffer = false;
		}
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, destination, effectMaterial);
	}

	private void OnDestroy()
	{
		if (blackwhiteBuffer != null) blackwhiteBuffer.Dispose();
		if (blackwhiteBufferTmp != null) blackwhiteBufferTmp.Dispose();
	}
}

public static class ScreenCapture {

    public static void TakeScreenshotOfMap()
    {
        TakeScreenshotOfMap(Application.dataPath);    
    }
    public static void TakeScreenshotOfMap(string path) {
        if (MainScript.Instance != null) {
            MainScript.Instance.TakeScreenshot(path);
        }
    }
}
